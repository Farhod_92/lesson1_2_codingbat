package uzb.farhod.lesson1_2_codingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson1_2_codingbat.entity.Example;
import uzb.farhod.lesson1_2_codingbat.entity.Task;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.ExampleDto;
import uzb.farhod.lesson1_2_codingbat.payload.TaskDto;
import uzb.farhod.lesson1_2_codingbat.repository.ExampleRepository;
import uzb.farhod.lesson1_2_codingbat.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ExampleService {
    @Autowired
    ExampleRepository exampleRepository;

    @Autowired
    TaskRepository taskRepository;

    public ApiResponse add(ExampleDto exampleDto){
        Optional<Task> optionalTask = taskRepository.findById(exampleDto.getTaskId());
        if(!optionalTask.isPresent())
            return new ApiResponse("ushbun id li  task topilmadi", false);

        try{
            Example example = new Example();
            example.setText(exampleDto.getText());
            example.setTask(optionalTask.get());
            exampleRepository.save(example);
            return new ApiResponse("yangi task qo'shildi", true);
        }catch (Exception e){
            return new ApiResponse("xatolik yangi task qo'shilmadi", false);
        }
    }

    public List<Example> getList(){
        return exampleRepository.findAll();
    }

    public Example getByID(Integer id){
        Optional<Example> optionalTheme = exampleRepository.findById(id);
        return optionalTheme.orElse(null);
    }

    public ApiResponse edit(Integer id, ExampleDto exampleDto){
        Optional<Example> optionalExample = exampleRepository.findById(id);

        if(!optionalExample.isPresent())
            return new ApiResponse("ushbu idli example topilmadi",false);

        Optional<Task> optionalTask = taskRepository.findById(exampleDto.getTaskId());
        if(!optionalTask.isPresent())
            return new ApiResponse("ushbun id li  task topilmadi", false);

        Example example = optionalExample.get();
        example.setText(exampleDto.getText());
        example.setTask(optionalTask.get());
        exampleRepository.save(example);
        return new ApiResponse("example tahrirlandi",true);
    }

    public ApiResponse delete(Integer id){
        try{
            exampleRepository.deleteById(id);
            return new ApiResponse("example o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("xatolik example o'chirilmadi", false);
        }
    }
}
