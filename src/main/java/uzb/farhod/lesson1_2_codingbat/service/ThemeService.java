package uzb.farhod.lesson1_2_codingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson1_2_codingbat.entity.ProgrammingLanguage;
import uzb.farhod.lesson1_2_codingbat.entity.Theme;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.ThemeDto;
import uzb.farhod.lesson1_2_codingbat.repository.ThemeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ThemeService {
    @Autowired
    ThemeRepository themeRepository;

    public ApiResponse add(ThemeDto themeDto){
        Theme theme=new Theme();
        theme.setName(themeDto.getName());
        ProgrammingLanguage programmingLanguage = ProgrammingLanguage.getById(themeDto.getProgrammingLanguage());
        theme.setProgrammingLanguage(programmingLanguage);
        themeRepository.save(theme);
        return new ApiResponse("theme qo'shildi",true);
    }

    public List<Theme> getList(){
        return themeRepository.findAll();
    }

    public Theme getByID(Integer id){
        Optional<Theme> optionalTheme = themeRepository.findById(id);
        return optionalTheme.orElse(null);
    }

    public ApiResponse edit(Integer id, ThemeDto themeDto){
        Optional<Theme> themeOptional = themeRepository.findById(id);

        if(!themeOptional.isPresent())
            return new ApiResponse("ushbu idli theme topilmadi",false);

        Theme theme=themeOptional.get();
        theme.setName(themeDto.getName());
        ProgrammingLanguage programmingLanguage = ProgrammingLanguage.getById(themeDto.getProgrammingLanguage());
        theme.setProgrammingLanguage(programmingLanguage);
        themeRepository.save(theme);
        return new ApiResponse("theme tahrirlandi",true);
    }

    public ApiResponse delete(Integer id){
        try{
            themeRepository.deleteById(id);
            return new ApiResponse("theme o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("xatolik theme o'chirilmadi", false);
        }
    }
}
