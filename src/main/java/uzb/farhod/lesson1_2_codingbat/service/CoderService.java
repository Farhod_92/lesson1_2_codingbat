package uzb.farhod.lesson1_2_codingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson1_2_codingbat.entity.Coder;
import uzb.farhod.lesson1_2_codingbat.entity.Example;
import uzb.farhod.lesson1_2_codingbat.entity.Task;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.CoderDto;
import uzb.farhod.lesson1_2_codingbat.payload.ExampleDto;
import uzb.farhod.lesson1_2_codingbat.repository.CoderRepository;
import uzb.farhod.lesson1_2_codingbat.repository.ExampleRepository;
import uzb.farhod.lesson1_2_codingbat.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CoderService {
    @Autowired
    CoderRepository coderRepository;

    public ApiResponse add(CoderDto coderDto){

        try{
            Coder coder = new Coder();
            coder.setEmail(coderDto.getEmail());
            coder.setPassword(coderDto.getPassword());
            coderRepository.save(coder);
            return new ApiResponse("yangi coder qo'shildi", true);
        }catch (Exception e){
            return new ApiResponse("xatolik yangi coder qo'shilmadi", false);
        }
    }

    public List<Coder> getList(){
        return coderRepository.findAll();
    }

    public Coder getByID(Integer id){
        Optional<Coder> optionalTheme = coderRepository.findById(id);
        return optionalTheme.orElse(null);
    }

    public ApiResponse edit(Integer id, CoderDto coderDto){
        Optional<Coder> optionalCoder = coderRepository.findById(id);

        if(!optionalCoder.isPresent())
            return new ApiResponse("ushbu idli coder topilmadi",false);

        Coder coder = optionalCoder.get();
        coder.setEmail(coderDto.getEmail());
        coder.setPassword(coderDto.getPassword());
        coderRepository.save(coder);
        return new ApiResponse("coder tahrirlandi",true);
    }

    public ApiResponse delete(Integer id){
        try{
            coderRepository.deleteById(id);
            return new ApiResponse("coder o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("xatolik coder o'chirilmadi", false);
        }
    }
}
