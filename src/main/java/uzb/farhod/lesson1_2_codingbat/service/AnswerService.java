package uzb.farhod.lesson1_2_codingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson1_2_codingbat.entity.Answer;
import uzb.farhod.lesson1_2_codingbat.entity.Coder;
import uzb.farhod.lesson1_2_codingbat.entity.Task;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.AnswerDto;
import uzb.farhod.lesson1_2_codingbat.repository.AnswerRepository;
import uzb.farhod.lesson1_2_codingbat.repository.CoderRepository;
import uzb.farhod.lesson1_2_codingbat.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AnswerService {
    @Autowired
    CoderRepository coderRepository;

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    AnswerRepository answerRepository;

    public ApiResponse addAnswer(AnswerDto answerDto){
        Optional<Coder> optionalCoder = coderRepository.findById(answerDto.getCoderId());
        if(!optionalCoder.isPresent())
            return new ApiResponse("bunaqa idli coder topilmadi", false);

        Optional<Task> optionalTask = taskRepository.findById(answerDto.getTaskId());
        if(!optionalTask.isPresent())
            return new ApiResponse("bunaqa idli task topilmadi", false);

        try{
            Answer answer=new Answer();
            answer.setAnswer(answerDto.getAnswer());
            answer.setCorrect(answerDto.isCorrect());
            answer.setCoder(optionalCoder.get());
            answer.setTask(optionalTask.get());
            answerRepository.save(answer);
            return new ApiResponse("answer qo'shildi", true);
        }catch (Exception e){
            return new ApiResponse("answer qo'shilmadi", false);
        }
    }

    public List<Task> getCoderTasks(Integer coderId) {
        Optional<Coder> optionalCoder = coderRepository.findById(coderId);
        if(!optionalCoder.isPresent())
                return null;
        List<Task> coderTasks=new ArrayList<>();
        optionalCoder.get().getAnswers().forEach(answer -> {
            coderTasks.add(answer.getTask());
        });

        return coderTasks;
    }

    public List<Coder> getTaskCoders(Integer taskId) {
        Optional<Task> optionalTask = taskRepository.findById(taskId);
        if(!optionalTask.isPresent())
            return null;

        List<Coder> taskCoders=new ArrayList<>();
        optionalTask.get().getAnswers().forEach(answer -> {
            taskCoders.add(answer.getCoder());
        });

        return taskCoders;
    }
}
