package uzb.farhod.lesson1_2_codingbat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uzb.farhod.lesson1_2_codingbat.entity.ProgrammingLanguage;
import uzb.farhod.lesson1_2_codingbat.entity.Task;
import uzb.farhod.lesson1_2_codingbat.entity.Theme;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.TaskDto;
import uzb.farhod.lesson1_2_codingbat.payload.ThemeDto;
import uzb.farhod.lesson1_2_codingbat.repository.TaskRepository;
import uzb.farhod.lesson1_2_codingbat.repository.ThemeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    @Autowired
    TaskRepository taskRepository;

    @Autowired
    ThemeRepository themeRepository;

    public ApiResponse add(TaskDto taskDto){
        Optional<Theme> optionalTheme = themeRepository.findById(taskDto.getThemeId());
        if(!optionalTheme.isPresent())
            return new ApiResponse("ushbu idli theme topilmadi", false);

        try{
            Task task = new Task();
            task.setTitle(taskDto.getTitle());
            task.setText(taskDto.getText());
            task.setTheme(optionalTheme.get());
            taskRepository.save(task);
            return new ApiResponse("yangi task qo'shildi", true);
        }catch (Exception e){
            return new ApiResponse("yangi task qo'shilmadi", false);
        }
    }

    public List<Task> getList(){
        return taskRepository.findAll();
    }

    public Task getByID(Integer id){
        Optional<Task> optionalTheme = taskRepository.findById(id);
        return optionalTheme.orElse(null);
    }

    public ApiResponse edit(Integer id, TaskDto taskDto){
        Optional<Task> taskOptional = taskRepository.findById(id);

        if(!taskOptional.isPresent())
            return new ApiResponse("ushbu idli task topilmadi",false);

        Task task=taskOptional.get();
        task.setTitle(taskDto.getTitle());
        task.setText(taskDto.getText());
        taskRepository.save(task);
        return new ApiResponse("task tahrirlandi",true);
    }

    public ApiResponse delete(Integer id){
        try{
            taskRepository.deleteById(id);
            return new ApiResponse("task o'chirildi", true);
        }catch (Exception e){
            return new ApiResponse("xatolik task o'chirilmadi", false);
        }
    }
}
