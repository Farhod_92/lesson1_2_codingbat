package uzb.farhod.lesson1_2_codingbat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lesson12CodingbatApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lesson12CodingbatApplication.class, args);
    }

}
