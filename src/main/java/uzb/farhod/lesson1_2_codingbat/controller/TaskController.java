package uzb.farhod.lesson1_2_codingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson1_2_codingbat.entity.Task;
import uzb.farhod.lesson1_2_codingbat.entity.Theme;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.TaskDto;
import uzb.farhod.lesson1_2_codingbat.payload.ThemeDto;
import uzb.farhod.lesson1_2_codingbat.service.TaskService;
import uzb.farhod.lesson1_2_codingbat.service.ThemeService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mavzularni ichidagi masalalarni crud qilish
 */
@RestController
@RequestMapping("/api/codingbat/task")
public class TaskController {
    @Autowired
    TaskService taskService;

    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody TaskDto taskDto){
       ApiResponse apiResponse = taskService.add(taskDto);
       return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<List<Task>> getList(){
        List<Task> list = taskService.getList();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public HttpEntity<Task> getById(@PathVariable Integer id){
        Task task = taskService.getByID(id);
        return ResponseEntity.status(task==null?409:200).body(task);
    }

    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> edit(@Valid @RequestBody TaskDto taskDto, @PathVariable Integer id){
        ApiResponse apiResponse = taskService.edit(id, taskDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> delete(@PathVariable Integer id){
        ApiResponse apiResponse = taskService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
