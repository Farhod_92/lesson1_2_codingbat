package uzb.farhod.lesson1_2_codingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson1_2_codingbat.entity.Example;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.ExampleDto;
import uzb.farhod.lesson1_2_codingbat.service.ExampleService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tasklarni boshida keltirilgan misollarni crud qilish
 */
@RestController
@RequestMapping("/api/codingbat/example")
public class ExampleController {
    @Autowired
    ExampleService exampleService;

    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody ExampleDto exampleDto){
       ApiResponse apiResponse = exampleService.add(exampleDto);
       return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<List<Example>> getList(){
        List<Example> list = exampleService.getList();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public HttpEntity<Example> getById(@PathVariable Integer id){
        Example example = exampleService.getByID(id);
        return ResponseEntity.status(example==null?409:200).body(example);
    }

    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> edit(@Valid @RequestBody ExampleDto exampleDto, @PathVariable Integer id){
        ApiResponse apiResponse = exampleService.edit(id, exampleDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> delete(@PathVariable Integer id){
        ApiResponse apiResponse = exampleService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
