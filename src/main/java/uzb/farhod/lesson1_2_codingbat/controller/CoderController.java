package uzb.farhod.lesson1_2_codingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson1_2_codingbat.entity.Coder;
import uzb.farhod.lesson1_2_codingbat.entity.Example;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.CoderDto;
import uzb.farhod.lesson1_2_codingbat.payload.ExampleDto;
import uzb.farhod.lesson1_2_codingbat.service.CoderService;
import uzb.farhod.lesson1_2_codingbat.service.ExampleService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * saytda ro'yxatdan o'tgan foydalanuvchilarni crud qilish
 */
@RestController
@RequestMapping("/api/codingbat/coder")
public class CoderController {
    @Autowired
    CoderService coderService;

    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody CoderDto coderDto){
       ApiResponse apiResponse = coderService.add(coderDto);
       return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<List<Coder>> getList(){
        List<Coder> list = coderService.getList();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public HttpEntity<Coder> getById(@PathVariable Integer id){
        Coder coder = coderService.getByID(id);
        return ResponseEntity.status(coder==null?409:200).body(coder);
    }

    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> edit(@Valid @RequestBody CoderDto coderDto,  @PathVariable Integer id){
        ApiResponse apiResponse = coderService.edit(id, coderDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> delete(@PathVariable Integer id){
        ApiResponse apiResponse = coderService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
