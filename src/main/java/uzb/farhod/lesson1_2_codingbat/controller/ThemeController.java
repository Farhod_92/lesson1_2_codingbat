package uzb.farhod.lesson1_2_codingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson1_2_codingbat.entity.Theme;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.payload.ThemeDto;
import uzb.farhod.lesson1_2_codingbat.service.ThemeService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * dasturlash tillari bo'yicha mavzu yoki bo'limlarni crud qilish
 */
@RestController
@RequestMapping("/api/codingbat/theme")
public class ThemeController {
    @Autowired
    ThemeService themeService;

    @PostMapping
    public HttpEntity<?> add(@Valid @RequestBody ThemeDto themeDto){
       ApiResponse apiResponse = themeService.add(themeDto);
       return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping
    public HttpEntity<List<Theme>> getList(){
        List<Theme> list = themeService.getList();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/{id}")
    public HttpEntity<Theme> getById(@PathVariable Integer id){
        Theme theme = themeService.getByID(id);
        return ResponseEntity.status(theme==null?409:200).body(theme);
    }

    @PutMapping("/{id}")
    public HttpEntity<ApiResponse> edit(@Valid @RequestBody ThemeDto themeDto, @PathVariable Integer id){
        ApiResponse apiResponse = themeService.edit(id, themeDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @DeleteMapping("/{id}")
    public HttpEntity<ApiResponse> delete(@PathVariable Integer id){
        ApiResponse apiResponse = themeService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
