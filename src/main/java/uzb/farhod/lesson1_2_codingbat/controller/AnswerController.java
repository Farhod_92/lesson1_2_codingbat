package uzb.farhod.lesson1_2_codingbat.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uzb.farhod.lesson1_2_codingbat.entity.Answer;
import uzb.farhod.lesson1_2_codingbat.entity.Coder;
import uzb.farhod.lesson1_2_codingbat.entity.Task;
import uzb.farhod.lesson1_2_codingbat.payload.AnswerDto;
import uzb.farhod.lesson1_2_codingbat.payload.ApiResponse;
import uzb.farhod.lesson1_2_codingbat.service.AnswerService;

import javax.validation.Valid;
import java.util.List;

/**
 * BU controller coder to'g'ri ishlagan masalalari ro'yxatini shakllantirish uchun
 * ma'lumotlar answer jadvaliga qo'shiladi
 */
@RestController
@RequestMapping("/api/codingbat/answer")
public class AnswerController {

    @Autowired
    AnswerService answerService;

    @PostMapping
    public HttpEntity<ApiResponse> addAnswer(@Valid @RequestBody AnswerDto answerDto){
        ApiResponse apiResponse = answerService.addAnswer(answerDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    /**
     * coder ishlagan tasklar ro'yxatini olish
     * @param coderId
     * @return List<Task>
     */
    @GetMapping("/coder/{coderId}")
    public HttpEntity<?> getCoderTasks(@PathVariable Integer coderId){
        List<Task> answers= answerService.getCoderTasks(coderId);
        return ResponseEntity.ok(answers);
    }

    /**
     * taskni ishlagan coderlar ro'yxatini olish
     * @param taskId
     * @return List<Task>
     */
    @GetMapping("/task/{taskId}")
    public HttpEntity<?> getTaskAnswers(@PathVariable Integer taskId){
        List<Coder> coders = answerService.getTaskCoders(taskId);
        return ResponseEntity.ok(coders);
    }
}
