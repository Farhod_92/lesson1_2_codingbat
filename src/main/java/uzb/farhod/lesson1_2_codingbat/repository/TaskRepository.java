package uzb.farhod.lesson1_2_codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson1_2_codingbat.entity.Task;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task,Integer> {
}
