package uzb.farhod.lesson1_2_codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson1_2_codingbat.entity.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Integer> {
}
