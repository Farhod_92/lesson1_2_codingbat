package uzb.farhod.lesson1_2_codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson1_2_codingbat.entity.Example;

public interface ExampleRepository extends JpaRepository<Example, Integer> {
}
