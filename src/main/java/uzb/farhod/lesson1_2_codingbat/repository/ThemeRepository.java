package uzb.farhod.lesson1_2_codingbat.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uzb.farhod.lesson1_2_codingbat.entity.Theme;

public interface ThemeRepository extends JpaRepository<Theme,Integer> {
}
