package uzb.farhod.lesson1_2_codingbat.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uzb.farhod.lesson1_2_codingbat.entity.Coder;
import uzb.farhod.lesson1_2_codingbat.entity.Example;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
public class TaskDto {

    @NotNull(message = "task title bo'sh")
    private String title;

    @NotNull(message = "task text bo'sh")
    private String text;

    private List<Coder> coder;

    private List<Example> example;

    @NotNull(message = "theme id bo'sh")
    private Integer themeId;


}
