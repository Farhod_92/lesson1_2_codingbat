package uzb.farhod.lesson1_2_codingbat.payload;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class AnswerDto {
    @NotNull(message = "answer  bo'sh")
    private String answer;

    @NotNull(message = "answer  bo'sh")
    private boolean correct;

    @NotNull(message = "task id bo'sh")
    private Integer taskId;

    @NotNull(message = "coder id bo'sh")
    private Integer coderId;
}
