package uzb.farhod.lesson1_2_codingbat.payload;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ThemeDto {
    @NotNull(message = "name bo'sh keldi")
    private String name;

    @NotNull(message = "programming language id bo'sh keldi")
    private Integer programmingLanguage;
}
