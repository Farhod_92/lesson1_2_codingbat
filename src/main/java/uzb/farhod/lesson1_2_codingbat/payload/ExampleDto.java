package uzb.farhod.lesson1_2_codingbat.payload;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ExampleDto {
    @NotNull(message = "example text bo'sh")
    private String text;

    @NotNull(message = "example task id bo'sh")
    private Integer taskId;
}
