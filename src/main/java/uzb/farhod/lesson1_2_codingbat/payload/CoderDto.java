package uzb.farhod.lesson1_2_codingbat.payload;

import lombok.Data;
import lombok.Getter;
import uzb.farhod.lesson1_2_codingbat.entity.Task;

import javax.persistence.Column;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
public class CoderDto {
    @NotNull(message = "email bo'sh")
    private String email;

    @NotNull(message = "password bo'sh")
    private String password;
}
