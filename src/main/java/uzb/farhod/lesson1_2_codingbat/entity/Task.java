package uzb.farhod.lesson1_2_codingbat.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String text;

    @JsonIgnore
    @OneToMany(mappedBy = "task")
    private List<Answer> answers;

    @OneToMany(mappedBy = "task")
    private List<Example> examples;

    @ManyToOne
    private Theme theme;
}
