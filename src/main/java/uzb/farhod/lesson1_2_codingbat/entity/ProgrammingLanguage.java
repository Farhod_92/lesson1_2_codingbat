package uzb.farhod.lesson1_2_codingbat.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProgrammingLanguage {
    UNKNOWN(0),
    JAVA(1),
    PYTHON(2);

    private final Integer id;

    public static ProgrammingLanguage getById(Integer id) {
        for(ProgrammingLanguage e : values()) {
            if(e.id.equals(id))
                return e;
        }
        return UNKNOWN;
    }

}
