package uzb.farhod.lesson1_2_codingbat.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aspectj.apache.bcel.classfile.Code;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String answer;

    private boolean correct;

    @ManyToOne
    private Coder coder;

    @ManyToOne
    private Task task;



}
